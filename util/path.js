const path = require('path');

// Path ini berfungsi agar kita selalu bisa melewati Root path menggantikan '../'
module.exports = path.dirname(process.mainModule.filename);
