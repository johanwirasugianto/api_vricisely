const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const sizeChartSchema = new Schema({
    sizeXXL:{
        type: Array,
        required: false
    },
    sizeXL:{
        type: Array,
        required: false
    },
    sizeL:{
        type: Array,
        required: false
    },
    sizeM:{
        type: Array,
        required: false
    },
    sizeS:{
        type: Array,
        required: false
    },
    sizeXS:{
        type: Array,
        required: false
    },
    sizeXXS:{
        type: Array,
        required: false
    }
});

module.exports = mongoose.model('SizeChart', sizeChartSchema);
