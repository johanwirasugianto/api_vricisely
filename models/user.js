const mongosee = require('mongoose');
const Schema = mongosee.Schema;

const userSchema = new Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type: String,
        required: true
    },
    password:{
        type: String,
        required: true
    },
    resetToken: String,
    resetTokenExpiration: Date,
});

module.exports = mongosee.model('User', userSchema);