const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const categoriesSchema = new Schema ({
    categoriesName:{
        type: String,
        require: true
    }
});

module.exports = mongoose.model('Categories', categoriesSchema);
