const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// const { ObjectId } = mongoose.Schema

const productSchema = new Schema({
    productName:{
        type: String,
        required: true
    },
    imagesProduct1:{
        type: String,
        required: false
    },
    imagesProduct2:{
        type: String,
        required: false
    },
    imagesProduct3:{
        type: String,
        required: false
    },
    imagesProduct4:{
        type: String,
        required: false
    },
    imagesProduct5:{
        type: String,
        required: false
    },
    imagesProduct6:{
        type: String,
        required: false
    },
    imagesProduct7:{
        type: String,
        required: false
    },
    imagesProduct8:{
        type: String,
        required: false
    },
    description:{
        type: String,
        required: true
    },
    color:{
        type: String,
        required: true
    },sizeXXL:{
        type: Array,
        required: false
    },
    sizeXL:{
        type: Array,
        required: false
    },
    sizeL:{
        type: Array,
        required: false
    },
    sizeM:{
        type: Array,
        required: false
    },
    sizeS:{
        type: Array,
        required: false
    },
    sizeXS:{
        type: Array,
        required: false
    },
    sizeXXS:{
        type: Array,
        required: false
    },
    tokopedia:{
        type: String,
        required: false
    },
    shopee:{
        type: String,
        required: false
    },
    bukalapak:{
        type: String,
        required: false
    },
    instagram:{
        type: String,
        required: false
    },
    userId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    brandId:{
        type: Schema.Types.ObjectId,
        ref: 'Brand',
    },
    categoryId:{
        type: Schema.Types.ObjectId,
        ref: 'Categories',
    }
});

module.exports = mongoose.model('Product', productSchema);



// const mongoose = require('mongoose');

// const Schema = mongoose.Schema;

// const productSchema = new Schema({
//     productName:{
//         type: String,
//         required: true
//     },
//     imageUrl1:{
//         type: String,
//         required: false
//     },
//     imageUrl2:{
//         type: String,
//         required: false
//     },
//     imageUrl3:{
//         type: String,
//         required: false
//     },
//     imageUrl4:{
//         type: String,
//         required: false
//     },
//     imageUrl5:{
//         type: String,
//         required: false
//     },
//     imageUrl6:{
//         type: String,
//         required: false
//     },
//     imageUrl7:{
//         type: String,
//         required: false
//     },
//     imageUrl8:{
//         type: String,
//         required: false
//     },
//     imageUrl9:{
//         type: String,
//         required: false
//     },
//     imageUrl10:{
//         type: String,
//         required: false
//     },
//     description:{
//         type: String,
//         required: true
//     },
//     color:{
//         type: String,
//         required: true
//     },
//     tokopedia:{
//         type: String,
//         required: false
//     },
//     shopee:{
//         type: String,
//         required: false
//     },
//     bukalapak:{
//         type: String,
//         required: false
//     },
//     instagram:{
//         type: String,
//         required: false
//     },
//     userId:{
//         type: Schema.Types.ObjectId,
//         ref: 'User',
//     },
//     brandId:{
//         type: Schema.Types.ObjectId,
//         ref: 'Brand',
//     },
//     sizeChartId:{
//         type: Schema.Types.ObjectId,
//         ref: 'SizeChart'
//     },
//     categoriesId:{
//         type: Schema.Types.ObjectId,
//         ref: 'Categories'
//     }
// });

// module.exports = mongoose.model('Product', productSchema);








