const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const brandSchema = new Schema ({
    brandName:{
        type: String,
        require: true
    },
    brandImage:{
        type: String,
        require: false
    },
    brandBanner1: {
        type: String,
        require: false
    },
    brandBanner2: {
        type: String,
        require: false
    },
    brandBanner3: {
        type: String,
        require: false
    },
    userId:{
        type: Schema.Types.ObjectId,
        ref: 'User',
    }
});

module.exports = mongoose.model('Brand', brandSchema);

// const mongodb = require('mongodb');
// const getDb = require('../util/database').getDb;

// class Brand{
//     constructor(brandName, brandImage){
//         this.brandName = brandName;
//         this.brandImage = brandImage;
//     }

//     save() {
//         const db = getDb();
//         return db.collection('brands')
//         .insertOne(this)
//         .then(result => {
//             console.log(result);
//         })
//         .catch(err => {
//             console.log(err);
//         });
//     }

//     static fatchAll() {
//         const db = getDb();
//         return db.collection('brands')
//         .find()
//         .toArray()
//         .then(product => {
//             console.log(product);
//             return product;
//         })
//         .catch(err => {
//             console.log(err);
//         });
//     }

//     static findById(brandId){
//         const db = getDb();
//         return db.collection('brands')
//         .find({ _id: mongodb.ObjectID(brandId) })
//         .next()
//         .then(product => {
//             console.log(product);
//             return product;
//         })
//         .catch(err => {
//             console.log(err);
//         });
//     }

//     // static findId(brandChoose){
//     //     const db = getDb();
//     //     return db.collection('brands')
//     //     .find({ brandName: brandChoose })
//     //     .next()
//     //     .then(product => {
//     //         console.log(product);
//     //         return product;
//     //     })
//     //     .catch(err => {
//     //         console.log(err);
//     //     });
//     // }

// }

// module.exports = Brand;