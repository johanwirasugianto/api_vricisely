const multer = require('multer');

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');  
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toString() + '-' + file.originalname);
    }
});

const fileFilter = (req, file, cb)=> { 
    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb(null, true);
    }
    else {
        cb(null, false);
    }
}

var uploadWakeh = multer({
    storage: fileStorage
}).fields([
    {name: 'imagesProduct1'},
    {name: 'imagesProduct2'},
    {name: 'imagesProduct3'},
    {name: 'imagesProduct4'},
    {name: 'imagesProduct5'},
    {name: 'imagesProduct6'},
    {name: 'imagesProduct7'},
    {name: 'imagesProduct8'},
])



module.exports = uploadWakeh;