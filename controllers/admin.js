//Mengambil product dari models
const Product = require('../models/dataProduct');

//Fungsi routes ketika melanjutkan http ke view admin/add-product
exports.getAddProduct = (req, res, next) => {
    res.render('admin/add-product', {
        pageTitle: 'Admin Add Product', 
        path:'/admin/add-product'
    });
}

//Fungsi routes ketika melakukan pengambilan data dari view user
exports.postAddProduct = (req, res, next) => {
    const productName = req.body.productName;
    const imageUrl = req.body.imageUrl;
    const description = req.body.description;
    const price = req.body.price;
    const product = new Product(productName, imageUrl, description, price);
    product.save();
    res.redirect('/');
}

//Fungsi routes ketika melanjutkan http ke view admin/product
exports.getProduct = (req, res, next) => {
    Product.find()
    .then(product =>{
        console.log(product);
        res.render('admin/products', {
            prods: product, 
            pageTitle: 'Admin Products', 
            path: '/admin/products'
        });
    })
    .catch(err => console.log(err));
}