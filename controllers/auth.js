const crypto = require('crypto');

const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const { validationResult } = require('express-validator/check');

const User = require('../models/user');

const transporter = nodemailer.createTransport(sendgridTransport({
    auth:{
        api_key: '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' 
    }
}));

exports.getLogin = (req, res, next) => {  
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    }else {
        message = null;
    }
    console.log(req.session.isLoggedIn);
    res.render('auth/first', {
        pageTitle: 'Login Page', 
        path: '/login',
        isAuthenticated: false,
        errorMessage: message,
        oldInput: {
            email: '',
            password: ''
        },
        validationErrors: []
    });
};

exports.getSignup = (req, res, next) => { 
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    }else {
        message = null;
    } 
   res.render('auth/signup', {
       path: '/signup',
       pageTitle: 'Signup Page',
       isAuthenticated: false,
       errorMessage: message,
       oldInput: { 
        name:'',
        email: '',
        password: '',
        confirmPassword: '',
        },
    validationErrors: []
   });
};

exports.postLogin = (req, res, next) => { 
    const email = req.body.email;
    const password = req.body.password;

    const errors = validationResult(req);
    if (!errors.isEmpty()){
     return res.status(422).render('auth/first', {
         path: '/login',
         pageTitle: 'Login Page',
         isAuthenticated: false,
         errorMessage: errors.array()[0].msg,
         oldInput: {
             email: email,
             password: password
         },
         validationErrors: errors.array()
     });
    }

    User.findOne({email: email})
    .then(user => {
        if (!user) {
            return res.status(422).render('auth/first', {
                path: '/login',
                pageTitle: 'Login Page',
                isAuthenticated: false,
                errorMessage: 'Invalid email or password',
                oldInput: {
                    email: email,
                    password: password
                },
                validationErrors: []
            });
        }
        bcrypt.compare(password, user.password)
        .then(passValid => {
            if (passValid){
                req.session.isLoggedIn = true;
                req.session.user = user;
                return req.session.save(err => {
                    console.log(err);
                    res.redirect('/');
                });
            }
            return res.status(422).render('auth/first', {
                path: '/login',
                pageTitle: 'Login Page',
                isAuthenticated: false,
                errorMessage: 'password has be valid',
                oldInput: {
                    email: email,
                    password: password
                },
                validationErrors: []
            });
        })
        .catch(err => {
            console.log(err);
            res.redirect('/login');
        });
    }).catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }); 
    // req.isLoggedIn = true;   
    // res.setHeader('Set-Cookie', 'loggedIn=true; HttpOnly');
}; 

exports.postSignup = (req, res, next) => { 
   const email = req.body.email;
   const name = req.body.name;
   const password = req.body.password;
   const confirmPassword = req.body.confirmPassword;

   const errors = validationResult(req);
   if (!errors.isEmpty()){
       console.log(errors.array());
    return res.status(422).render('auth/signup', {
        path: '/signup',
        pageTitle: 'Signup Page',
        isAuthenticated: false,
        errorMessage: errors.array()[0].msg,
        oldInput: { 
            name: name,
            email: email ,
            password: password,
            confirmPassword: confirmPassword 
        },
        validationErrors : errors.array()
    });
   }
    bcrypt
    .hash(password, 12) 
        .then(hashedPassword => {
            const user = new User ({
                name: name,
                email: email,
                password: hashedPassword
            });
            return user.save();
        })
        .then(result => {
            res.redirect('/login');
            return transporter.sendMail({
                to: email,
                from: 'johanwirasugianto@gmail.com',
                subject: 'We are from Vricisely',
                html: '<h1> You seccessfully signed up </h1> <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRlAHSjq7Gk3-FboSL_OMkC42bdCkxD12e4mw&usqp=CAU">'
            })
        })
        .catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}; 

exports.postLogout = (req, res, next) => { 
    req.session.destroy(err => {
    console.log(err);
    res.redirect('/');
   });
}; 


exports.getReset = (req, res, next) => {
    let message = req.flash('error');
    if (message.length > 0) {
        message = message[0];
    }else {
        message = null;
    }
    res.render('auth/reset', {
        pageTitle: 'Reset Password ', 
        path: '/reset',
        isAuthenticated: false,
        errorMessage: message
    });
};

exports.postReset = (req, res, next) => {
    crypto.randomBytes(32, (err, buffer) => {
        if (err){
            console.log(err);
            return res.redirect('/reset');
        }
        const token = buffer.toString('hex');
        User.findOne({email: req.body.email})
        .then(user => {
            if(!user){
                req.flash('error', 'No account with the E-Mail found.');
                return res.redirect('/reset');
            }
            user.resetToken = token;
            user.resetTokenExpiration = Date.now() + 3600000;
            return user.save();
        })
        .then(result => {
            res.redirect('/');
            transporter.sendMail({
                to: req.body.email,
                from: 'johanwirasugianto@gmail.com',
                subject: 'Password Reset',
                html: `
                    <p> You requested to reset password </p>
                    <p> Click this <a href="http://localhost:3000/reset/${token}">link</a> to set a new password</p>
                `
            })
        })
        .catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
    });
};

exports.getNewPassword = (req, res, next) => {
    const token = req.params.token;
    User.findOne({resetToken: token, resetTokenExpiration: {$gt: Date.now()}})
    .then(user => {
        let message = req.flash('error');
        if (message.length > 0) {
            message = message[0];
        }else {
            message = null;
        }
        res.render('auth/new-password', {
            pageTitle: 'New Password ', 
            isAuthenticated: false,
            errorMessage: message,
            path: '/new-password',
            userId: user._id.toString(),
            passwordToken: token
        });
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

exports.postNewPassword = (req, res, next) => {
    const newPassword = req.body.password;
    const userId = req.body.userId;
    const passwordToken = req.body.passwordToken;
    let resetUser;
    User.findOne({
        resetToken: passwordToken,
        resetTokenExpiration: {$gt: Date.now()},
        _id: userId
    })
    .then(user => {
        resetUser = user;
        return bcrypt.hash(newPassword, 12);
    })
    .then(hashedPassword => {
        resetUser.password = hashedPassword;
        resetUser.resetToken = undefined;
        resetUser.resetTokenExpiration = undefined;
        return resetUser.save();
    })
    .then(result => {
        console.log('Reset Password succeed');
        res.redirect('/login');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
};