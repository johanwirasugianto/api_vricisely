//Mengambil product dari models
// const monggodb = require('mongodb');
const Product = require('../models/dataProduct');
const Brand = require('../models/brand');
const Category = require('../models/categories');
const mongoose = require('mongoose');
const fileHelper = require('../util/file');

const { validationResult } = require('express-validator/check');
// const BrandItem = require('../models/brandItem');

////=============== HOME ==================\\\\
exports.getHome = (req, res, next) => {
    Brand.find()
    .then(brands => {
        res.render('seller/home', {
            brand: brands, 
            pageTitle: 'HomePage', 
            path: '/'
        });
    }).catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

exports.postAddBrand = (req, res, next) => {
    const brandName = req.body.brandName;
    const brandFoto = req.file;
    const brandBanner1 = "";
    const brandBanner2 = "";
    const brandBanner3 = "";
   
    const brandImage1 = brandFoto.path;

    const brand = new Brand ({
        brandName : brandName,
        brandImage: brandImage1,
        brandBanner1: brandBanner1,
        brandBanner2: brandBanner2,
        brandBanner3: brandBanner3,
        userId: req.session.user
    });
    brand.save()
    .then(result => {
        console.log(result);
        console.log('Created Brand');
        res.redirect('/home');
    }).catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

////=============== END HOME ==================\\\\


////=============== STROE (DAFTAR BRAND) ==================\\\\
exports.getStore = (req, res, next) => {
    const brandsId = req.params.brandId;
    let brandData;
    let categoryData;
    Brand.findById(brandsId)
    .then(brand => {
        brandData = brand
        return  Category.find()
    })
    .then(category =>{
        categoryData = category
        return  Product.find({brandId: brandsId})
    })
   
    .then(product => {
        if(brandData && categoryData){
            res.render('seller/store', {
                categorys:categoryData,
                brands: brandData,
                products: product,
                pageTitle: 'Store',
                path: '/store'
            });
        }
    })
    .catch(err => {
        // console.log(err);
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    })
}

exports.getBrandProfile = (req, res, next) => {
    const brandsId = req.params.brandId;
    var productData;
    Product.find({brandId: brandsId})
    .then(product => {
        productData = product;
        return Brand.findById(brandsId);
    })
    .then(brand => {
        res.render('seller/profile', {
            products: productData,
            brands: brand,
            pageTitle: 'Brand Profile',
            path: '/brandProfile',
        });
    })
    .catch(err => {
        console.log(err);
    })
}

exports.postEditBrandProfile = (req, res, next) => {
    const brandsId = req.body.brandId;
    const image = req.file;
    const brandName = req.body.brandName;
    console.log('Masuk');
    Brand.findById(brandsId)
    .then(brand => {
        brand.brandName = brandName;
        if(image){
            fileHelper.deleteFile(brand.brandImage);
            brand.brandImage = image.path;
        }
        brand.save()
        .then(result => {
            console.log('UPDATED Brand');
            res.redirect('/store/'+brandsId);
        });
    })
    .catch(err => {
        console.log(err);
    })
}

exports.postBrandBanner1 = (req, res, next) => {
    const brandsId = req.body.brandId;
    const brandBanners1 = req.file;
    const banner = brandBanners1.path;
    Brand.findById(brandsId)
    .then(brand => {
        brand.brandBanner1 = banner;
        brand.save()
        .then(result => {
            console.log('UPDATE Banner1');
            res.redirect('/brandProfile/'+brandsId);
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postBrandBanner2 = (req, res, next) => {
    const brandsId = req.body.brandId;
    const brandBanners2 = req.file;
    const banner = brandBanners2.path;
    Brand.findById(brandsId)
    .then(brand => {
        brand.brandBanner2 = banner;
        brand.save()
        .then(result => {
            console.log('UPDATE Banner1');
            res.redirect('/brandProfile/'+brandsId);
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postBrandBanner3 = (req, res, next) => {
    const brandsId = req.body.brandId;
    const brandBanners3 = req.file;
    const banner = brandBanners3.path;
    Brand.findById(brandsId)
    .then(brand => {
        brand.brandBanner3 = banner;
        brand.save()
        .then(result => {
            console.log('UPDATE Banner1');
            res.redirect('/brandProfile/'+brandsId);
        });
    })
    .catch(err => {
        console.log(err);
    })
}

exports.postDeleteBanner1 = (req, res, next) => {
    const brandId = req.body.brandId;
    Brand.findById(brandId)
    .then(brand =>{
        if(!brand.brandBanner1){
            return next(new Error('Banner1 Not Found'));
        }
        fileHelper.deleteFile(brand.brandBanner1);
        brand.brandBanner1 = "";
        return brand.save();
    })
    .then(() => {
        console.log('DESTROYED Banner1');
        res.redirect('/brandProfile/'+brandId);
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteBanner2 = (req, res, next) => {
    const brandId = req.body.brandId;
    Brand.findById(brandId)
    .then(brand =>{
        if(!brand.brandBanner2){
            return next(new Error('Banner1 Not Found'));
        }
        fileHelper.deleteFile(brand.brandBanner2);
        brand.brandBanner2 = "";
        return brand.save();
    })
    .then(() => {
        console.log('DESTROYED Banner2');
        res.redirect('/brandProfile/'+brandId);
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteBanner3 = (req, res, next) => {
    const brandId = req.body.brandId;
    Brand.findById(brandId)
    .then(brand =>{
        if(!brand.brandBanner3){
            return next(new Error('Banner1 Not Found'));
        }
        fileHelper.deleteFile(brand.brandBanner3);
        brand.brandBanner3 = "";
        return brand.save();
    })
    .then(() => {
        console.log('DESTROYED Banner3');
        res.redirect('/brandProfile/'+brandId);
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
////=============== END STORE(BRANDDD) ==================\\\\


/////===============  ADD PRODUCT ==================\\\\

exports.getAddProduct = (req, res, next) => {
    const brandsId = req.params.brandId;
    Category.find()
    .then(category => {
        res.render('seller/newProd', {
            brandId: brandsId,
            pageTitle: 'Add Products',
            path: '/addProduct',
            categorys: category,
            editing: false,
        });
    })
   .catch(err =>{
       console.log(err);
   })
}
exports.postaddProduct = (req, res, next) => {
    const brandId = req.body.brandId;
    const categoryId = req.body.categoryId;
    const productName = req.body.productName;
    // const image = req.file;
    const description = req.body.description;
    const color = req.body.color;
    const tokopedia = req.body.tokopedia;
    const shopee = req.body.shopee;
    const bukalapak = req.body.bukalapak;
    const instagram = req.body.instagram;

    // const imageUrl = image.path;

    //SHOULDER 
    const shoulderXXS = req.body.shoulderXXS;
    const shoulderXS = req.body.shoulderXS;
    const shoulderS = req.body.shoulderS;
    const shoulderM = req.body.shoulderM;
    const shoulderL = req.body.shoulderL;
    const shoulderXL = req.body.shoulderXL;
    const shoulderXXL = req.body.shoulderXXL;
    
    //Chest
    const chestXXS = req.body.chestXXS;
    const chestXS = req.body.chestXS;
    const chestS = req.body.chestS;
    const chestM = req.body.chestM;
    const chestL = req.body.chestL;
    const chestXL = req.body.chestXL;
    const chestXXL = req.body.chestXXL;

    //length
    const lengthXXS = req.body.lengthXXS;
    const lengthXS = req.body.lengthXS;
    const lengthS = req.body.lengthS;
    const lengthM = req.body.lengthM;
    const lengthL = req.body.lengthL;
    const lengthXL = req.body.lengthXL;
    const lengthXXL = req.body.lengthXXL;

    //Sleve Length
    const sleveLengthXXS = req.body.sleveLengthXXS;
    const sleveLengthXS = req.body.sleveLengthXS;
    const sleveLengthS = req.body.sleveLengthS;
    const sleveLengthM = req.body.sleveLengthM;
    const sleveLengthL = req.body.sleveLengthL;
    const sleveLengthXL = req.body.sleveLengthXL;
    const sleveLengthXXL = req.body.sleveLengthXXL;

    //Waist
    const waistXXS = req.body.waistXXS;
    const waistXS = req.body.waistXS;
    const waistS = req.body.waistS;
    const waistM = req.body.waistM;
    const waistL = req.body.waistL;
    const waistXL = req.body.waistXL;
    const waistXXL = req.body.waistXXL;

    //Hip
    const hipXXS = req.body.hipXXS;
    const hipXS = req.body.hipXS;
    const hipS = req.body.hipS;
    const hipM = req.body.hipM;
    const hipL = req.body.hipL;
    const hipXL = req.body.hipXL;
    const hipXXL = req.body.hipXXL;

    //Tight
    const tightXXS = req.body.tightXXS;
    const tightXS = req.body.tightXS;
    const tightS = req.body.tightS;
    const tightM = req.body.tightM;
    const tightL = req.body.tightL;
    const tightXL = req.body.tightXL;
    const tightXXL = req.body.tightXXL;

    //Front rise
    const frontRiseXXS = req.body.frontRiseXXS;
    const frontRiseXS = req.body.frontRiseXS;
    const frontRiseS = req.body.frontRiseS;
    const frontRiseM = req.body.frontRiseM;
    const frontRiseL = req.body.frontRiseL;
    const frontRiseXL = req.body.frontRiseXL;
    const frontRiseXXL = req.body.frontRiseXXL;

    //Back Rise
    const backRiseXXS = req.body.backRiseXXS;
    const backRiseXS = req.body.backRiseXS;
    const backRiseS = req.body.backRiseS;
    const backRiseM = req.body.backRiseM;
    const backRiseL = req.body.backRiseL;
    const backRiseXL = req.body.backRiseXL;
    const backRiseXXL = req.body.backRiseXXL;

    
    const product = new Product({
        productName: productName,
        description: description,
        color: color,
        sizeXXL: [shoulderXXL, chestXXL, lengthXXL, sleveLengthXXL, waistXXL, hipXXL, tightXXL, frontRiseXXL, backRiseXXL],
        sizeXL: [shoulderXL, chestXL, lengthXL, sleveLengthXL, waistXL, hipXL, tightXL, frontRiseXL, backRiseXL],
        sizeL: [shoulderL, chestL, lengthL, sleveLengthL, waistL, hipL, tightL, frontRiseL, backRiseL],
        sizeM: [shoulderM, chestM, lengthM, sleveLengthM, waistM, hipM, tightM, frontRiseM, backRiseM],
        sizeS: [shoulderS, chestS, lengthS, sleveLengthS, waistS, hipS, tightS, frontRiseS, backRiseS],
        sizeXS: [shoulderXS, chestXS, lengthXS, sleveLengthXS, waistXS, hipXS, tightXS, frontRiseXS, backRiseXS],
        sizeXXS: [shoulderXXS, chestXXS, lengthXXS, sleveLengthXXS, waistXXS, hipXXS, tightXXS, frontRiseXXS, backRiseXXS],
        tokopedia: tokopedia,
        shopee: shopee,
        bukalapak: bukalapak,
        instagram: instagram,
        userId: req.session.user,
        brandId: brandId,
        categoryId: categoryId
    });
    product.save()
    .then(result => {
        console.log('Created Product');
        res.redirect('/store/'+brandId);
    }).catch(err =>{
        //console.log(err);
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

exports.getEditProducts = (req, res, next) => {
    //untuk mngecek query pada html ?edit=true
    editMode = req.query.edit;
    if(!editMode){
        return res.redirect('/');
    }
    const proId = req.params.productId;
    const categoryId = req.params.categoryId;
    Category.find({_id: categoryId})
    .then(category => {
        categoryData = category;
        return Product.findById(proId)
    })
    .then(product => {
        if (!product){
            return res.redirect('/');       
        }
        res.render('seller/newProd', {
        pageTitle: 'Edit Product', 
        path: '/edit-products',
        editing: editMode,
        product: product,
        categorys: categoryData,
        // hasError: false,
        // errorMessage: null,
        // validationErrors: []
        });
    }).catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postEditProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const brandId = req.body.brandId;
    const productName = req.body.productName;
    const description = req.body.description;
    const color = req.body.color;
    const tokopedia = req.body.tokopedia;
    const shopee = req.body.shopee;
    const bukalapak = req.body.bukalapak;
    const instagram = req.body.instagram;

    //SHOULDER 
    const shoulderXXS = req.body.shoulderXXS;
    const shoulderXS = req.body.shoulderXS;
    const shoulderS = req.body.shoulderS;
    const shoulderM = req.body.shoulderM;
    const shoulderL = req.body.shoulderL;
    const shoulderXL = req.body.shoulderXL;
    const shoulderXXL = req.body.shoulderXXL;

    //Chest
    const chestXXS = req.body.chestXXS;
    const chestXS = req.body.chestXS;
    const chestS = req.body.chestS;
    const chestM = req.body.chestM;
    const chestL = req.body.chestL;
    const chestXL = req.body.chestXL;
    const chestXXL = req.body.chestXXL;

    //length
    const lengthXXS = req.body.lengthXXS;
    const lengthXS = req.body.lengthXS;
    const lengthS = req.body.lengthS;
    const lengthM = req.body.lengthM;
    const lengthL = req.body.lengthL;
    const lengthXL = req.body.lengthXL;
    const lengthXXL = req.body.lengthXXL;

    //Sleve Length
    const sleveLengthXXS = req.body.sleveLengthXXS;
    const sleveLengthXS = req.body.sleveLengthXS;
    const sleveLengthS = req.body.sleveLengthS;
    const sleveLengthM = req.body.sleveLengthM;
    const sleveLengthL = req.body.sleveLengthL;
    const sleveLengthXL = req.body.sleveLengthXL;
    const sleveLengthXXL = req.body.sleveLengthXXL;

    //Waist
    const waistXXS = req.body.waistXXS;
    const waistXS = req.body.waistXS;
    const waistS = req.body.waistS;
    const waistM = req.body.waistM;
    const waistL = req.body.waistL;
    const waistXL = req.body.waistXL;
    const waistXXL = req.body.waistXXL;

    //Hip
    const hipXXS = req.body.hipXXS;
    const hipXS = req.body.hipXS;
    const hipS = req.body.hipS;
    const hipM = req.body.hipM;
    const hipL = req.body.hipL;
    const hipXL = req.body.hipXL;
    const hipXXL = req.body.hipXXL;

    //Tight
    const tightXXS = req.body.tightXXS;
    const tightXS = req.body.tightXS;
    const tightS = req.body.tightS;
    const tightM = req.body.tightM;
    const tightL = req.body.tightL;
    const tightXL = req.body.tightXL;
    const tightXXL = req.body.tightXXL;

    //Front rise
    const frontRiseXXS = req.body.frontRiseXXS;
    const frontRiseXS = req.body.frontRiseXS;
    const frontRiseS = req.body.frontRiseS;
    const frontRiseM = req.body.frontRiseM;
    const frontRiseL = req.body.frontRiseL;
    const frontRiseXL = req.body.frontRiseXL;
    const frontRiseXXL = req.body.frontRiseXXL;

    //Back Rise
    const backRiseXXS = req.body.backRiseXXS;
    const backRiseXS = req.body.backRiseXS;
    const backRiseS = req.body.backRiseS;
    const backRiseM = req.body.backRiseM;
    const backRiseL = req.body.backRiseL;
    const backRiseXL = req.body.backRiseXL;
    const backRiseXXL = req.body.backRiseXXL;


    Product.findById(prodId)
        .then(product => {
            // if (product.userId.toString() !== req.user._id.toString()){
            //     return res.redirect('/')
            // }
            product.productName = productName;
            product.description = description;
            product.color = color;
            product.sizeXXL = [shoulderXXL, chestXXL, lengthXXL, sleveLengthXXL, waistXXL, hipXXL, tightXXL, frontRiseXXL, backRiseXXL];
            product.sizeXL = [shoulderXL, chestXL, lengthXL, sleveLengthXL, waistXL, hipXL, tightXL, frontRiseXL, backRiseXL];
            product.sizeL= [shoulderL, chestL, lengthL, sleveLengthL, waistL, hipL, tightL, frontRiseL, backRiseL];
            product.sizeM= [shoulderM, chestM, lengthM, sleveLengthM, waistM, hipM, tightM, frontRiseM, backRiseM];
            product.sizeS= [shoulderS, chestS, lengthS, sleveLengthS, waistS, hipS, tightS, frontRiseS, backRiseS];
            product.sizeXS= [shoulderXS, chestXS, lengthXS, sleveLengthXS, waistXS, hipXS, tightXS, frontRiseXS, backRiseXS];
            product.sizeXXS= [shoulderXXS, chestXXS, lengthXXS, sleveLengthXXS, waistXXS, hipXXS, tightXXS, frontRiseXXS, backRiseXXS];
            product.tokopedia= tokopedia;
            product.shopee= shopee;
            product.bukalapak= bukalapak;
            product.instagram= instagram;
            product.save()
            .then(result => {
                console.log('UPDATED PRODUCT');
                res.redirect('/store/'+brandId);
        });
        }).catch(err => {
            const error = new Error(err);
            error.httpStatusCode = 500;
            return next(error);
        });
}

exports.postDeleteProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const brandId = req.body.brandId;
    Product.findById(prodId)
    .then(product =>{
        if(!product){
            return next(new Error('Product Not Found'));
        }
        fileHelper.deleteFile(product.imageUrl);
        return Product.deleteOne({_id: prodId, userId: req.user._id})
    })
    .then(() => {
        console.log('DESTROYED PRODUCT');
        res.redirect('/store/'+brandId);
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

exports.postImagesProduct1 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const brandId = req.body.brandId;
    const images = req.file;
    const imagesProduct1 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct1 = imagesProduct1;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct1');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct2 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct2 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct2 = imagesProduct2;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct2');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct3 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct3 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct3 = imagesProduct3;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct3');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct4 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct4 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct4 = imagesProduct4;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct4');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct5 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct5 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct5 = imagesProduct5;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct5');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct6 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct6 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct6 = imagesProduct6;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct6');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct7 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct7 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct7 = imagesProduct7;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct7');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}
exports.postImagesProduct8 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    const images = req.file;
    const imagesProduct8 = images.path;
    Product.findById(prodId)
    .then(product => {
        product.imagesProduct8 = imagesProduct8;
        product.save()
        .then(result => {
            console.log('UPDATE ImageProduct8');
            res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
        });
    })
    .catch(err => {
        console.log(err);
    })
}

exports.postDeleteImagesProduct1 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct1){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct1);
        product.imagesProduct1 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct2 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct2){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct2);
        product.imagesProduct2 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct3 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct3){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct3);
        product.imagesProduct3 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct4 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct4){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct4);
        product.imagesProduct4 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct5 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct5){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct5);
        product.imagesProduct5 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct6 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct6){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct6);
        product.imagesProduct6 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct7 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct7){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct7);
        product.imagesProduct7 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}
exports.postDeleteImagesProduct8 = (req, res, next) => {
    const prodId = req.body.productId;
    const categoryId = req.body.categoryId;
    Product.findById(prodId)
    .then(product =>{
        if(!product.imagesProduct8){
            return next(new Error('imagesProduct Not Found'));
        }
        fileHelper.deleteFile(product.imagesProduct8);
        product.imagesProduct8 = "";
        return product.save();
    })
    .then(() => {
        console.log('DESTROYED images');
        res.redirect('/editProduct/'+prodId+'/'+categoryId+'?edit=true');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}


////=============== END ADD PRODUCT ==================\\\\


//Fungsi routes ketika melanjutkan http ke view /products
exports.getProducts = (req, res, next) => {
    Product.find({userId: req.user._id})
    .then(products => {
        console.log(products)
        res.render('seller/product-list', {
            prods: products, 
            pageTitle: 'Seller Product-list', 
            path: '/products'
        });
    }).catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

//Masuk ke Product Detail
exports.getProductId = (req, res, next) => {
    const prodId = req.params.productId;
    Product.findById(prodId)
    .then(product => {
        console.log(product);
        res.render('seller/product-detail', {
            product: product,
            pageTitle: 'Product Detail',
            path: '/products'
        });
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

//Fungsi routes ketika melanjutkan http ke view /
// exports.getIndex = (req, res, next) => {
//     Product.find()
//     .then(products => {
//         console.log(products);
//         res.render('seller/index', {
//             prods: products, 
//             pageTitle: 'HomePage', 
//             path: '/'
//         });
//     }).catch(err => {
//         const error = new Error(err);
//         error.httpStatusCode = 500;
//         return next(error);
//     });
// }

//Fungsi routes ketika melanjutkan http ke view /addProducts
exports.getAddProducts = (req, res, next) => {
    Brand.find()
    .then(brand => {
        res.render('seller/edit-product', {
            brands: brand,
            pageTitle: 'Seller Add Product', 
            path: '/add-product',
            editing: false,
            hasError: false,
            errorMessage: null,
            validationErrors: []
        });
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

//Fungsi routes ketika melakukan pengambilan data dari user
// exports.postAddProduct = (req, res, next) => {
//     const brandId = req.body.brandId;
//     const productName = req.body.productName;
//     const image = req.file;
//     const description = req.body.description;
//     const color = req.body.color;
//     const sizeXXL = req.body.sizeXXL;
//     const sizeXL = req.body.sizeXL;
//     const sizeL = req.body.sizeL;
//     const sizeM = req.body.sizeM;
//     const sizeS = req.body.sizeS;
//     const sizeXS = req.body.sizeXS;
//     const sizeXXS = req.body.sizeXXS;
//     const tokopedia = req.body.tokopedia;
//     const shopee = req.body.shopee;
//     const bukalapak = req.body.bukalapak;
//     const instagram = req.body.instagram;
//     const errors = validationResult(req);

//     if (!errors.isEmpty()){
//         Brand.find()
//         .then(brand => {
//             return res.status(422).render('seller/edit-product', {
//                 brands: brand,
//                 pageTitle: 'Seller Add Product', 
//                 path: '/add-product',
//                 editing: false,
//                 hasError: true,
//                 product: {
//                     productName: productName,
//                     // images: image,
//                     description: description,
//                     color: color,
//                     sizeXXL: sizeXXL,
//                     sizeXL: sizeXL,
//                     sizeL: sizeL,
//                     sizeM: sizeM,
//                     sizeS: sizeS,
//                     sizeXS: sizeXS,
//                     sizeXXS: sizeXXS,
//                     tokopedia: tokopedia,
//                     shopee: shopee,
//                     bukalapak: bukalapak,
//                     instagram: instagram
//                 },
//                 errorMessage: errors.array()[0].msg,
//                 validationErrors: errors.array()
//             });
//         })
//         .catch(err => console.log(err)); 
//     }

//     //di ubah ke path
//     const imageUrl = image.path;
//     console.log(imageUrl);

//     const product = new Product({
//        //_id: new mongoose.Types.ObjectId('5f87dd188669b909e6eeaca0'),
//         productName: productName,
//         imageUrl: imageUrl,
//         description: description,
//         color: color,
//         sizeXXL: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeXL: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeL: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeM: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeS: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeXS: [sizeXL, sizeL, sizeM, sizeS ],
//         sizeXXS: [sizeXL, sizeL, sizeM, sizeS ],
//         tokopedia: tokopedia,
//         shopee: shopee,
//         bukalapak: bukalapak,
//         instagram: instagram,
//         userId: req.session.user,
//         brandId: brandId
//     });
//     product.save()
//     .then(result => {
//         // console.log(result);
//         console.log('Created Product');
//         res.redirect('/');
//     }).catch(err =>{
//         // const error = new Error(err);
//         // error.httpStatusCode = 500;
//         // return next(error);
//     });
// }

//Menuju ke edit-product
// exports.getEditProducts = (req, res, next) => {
//     //untuk mngecek query pada html ?edit=true
//     editMode = req.query.edit;
//     if(!editMode){
//         return res.redirect('/');
//     }
//     //mengambil data dari link
//     const proId = req.params.productId;
//     Product.findById(proId)
//     .then(product => {
//         if (!product){
//             return res.redirect('/');       
//         }
//         res.render('seller/edit-product', {
//         pageTitle: 'Seller Edit Product', 
//         path: '/edit-products',
//         editing: editMode,
//         product: product,
//         hasError: false,
//         errorMessage: null,
//         validationErrors: []
//         });
//     }).catch(err => {
//         const error = new Error(err);
//         error.httpStatusCode = 500;
//         return next(error);
//     });
// }

// exports.postEditProduct = (req, res, next) => {
//     const prodId = req.body.productId;
//     const updateName = req.body.productName;
//     const updateColor = req.body.color;
//     const image = req.file;
//     const updateDesc = req.body.description;
//     const updateTokopedia = req.body.tokopedia;
//     const updateShopee = req.body.shopee;
//     const updateBukalapak = req.body.bukalapak;
//     const updateInstagram = req.body.instagram;
//     const updateSizeXL = req.body.sizeXL;
//     const updateSizeL = req.body.sizeL;
//     const updateSizeM = req.body.sizeM;
//     const updateSizeS = req.body.sizeS;
//     const errors = validationResult(req);

//     if (!errors.isEmpty()){
//         return res.status(422).render('seller/edit-product', {
//             pageTitle: 'Seller Edit Product', 
//             path: '/edit-product',
//             editing: true,
//             hasError: true,
//             product: {
//                 productName: updateName,
//                 description: updateDesc,
//                 color: updateColor,
//                 sizeXL: updateSizeXL,
//                 sizeL: updateSizeL,
//                 sizeM: updateSizeM,
//                 sizeS: updateSizeS,
//                 tokopedia: updateTokopedia,
//                 shopee: updateShopee,
//                 bukalapak: updateBukalapak,
//                 instagram: updateInstagram,
//                 _id: prodId
//             },
//             errorMessage: errors.array()[0].msg,
//             validationErrors: errors.array()
//         });
//     }

//     Product.findById(prodId)
//         .then(product => {
//             if (product.userId.toString() !== req.user._id.toString()){
//                 return res.redirect('/')
//             }
//             product.productName = updateName;
//             if(image){
//                 fileHelper.deleteFile(product.imageUrl);
//                 product.imageUrl = image.path;
//             }
//             product.description = updateDesc;
//             product.color = updateColor;
//             product.sizeXL = updateSizeXL;
//             product.sizeL = updateSizeL;
//             product.sizeM = updateSizeM;
//             product.sizeS = updateSizeS;
//             product.tokopedia = updateTokopedia;
//             product.shopee = updateShopee;
//             product.bukalapak = updateBukalapak;
//             product.instagram = updateInstagram;
//             product.save()
//             .then(result => {
//                 console.log('UPDATED PRODUCT');
//                 res.redirect('/');
//         });
//         }).catch(err => {
//             const error = new Error(err);
//             error.httpStatusCode = 500;
//             return next(error);
//         });
// }

//Product.findByIdAndDelete(prodId)
// exports.postDeleteProduct = (req, res, next) => {
//     const prodId = req.body.productId;
//     Product.findById(prodId)
//     .then(product =>{
//         if(!product){
//             return next(new Error('Product Not Found'));
//         }
//         fileHelper.deleteFile(product.imageUrl);
//         return Product.deleteOne({_id: prodId, userId: req.user._id})
//     })
//     .then(() => {
//         console.log('DESTROYED PRODUCT');
//         res.redirect('/');
//     })
//     .catch(err => {
//         const error = new Error(err);
//         error.httpStatusCode = 500;
//         return next(error);
//     });
// }

exports.getBrand = (req, res, next) =>{
    Brand.find()
    .then(brand => {
        res.render('seller/brand',{
            pageTitle: 'Brand',
            path:'/brand',
            brands: brand
        });
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}

exports.getAddBrand = (req, res, next) => {
    res.render('seller/add-brand',{
        pageTitle: 'Add-Brand',
        path:'/add-brand'
    });
}



exports.postDeleteBrand = (req, res, next) => {
    const brandId = req.body.brandId;
    Brand.findByIdAndDelete(brandId)
    .then(() => {
        console.log('DESTROYED BRAND');
        res.redirect('/brand');
    })
    .catch(err => {
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    });
}


///========CATEGORY==========\\\\


exports.getAddCategory = (req,res,next)=>{
    res.render('seller/add-category',{
        pageTitle: 'Add-Category',
        path:'/add-category'
    });
}

exports.postAddCategory = (req,res,next)=>{
    const categoryName = req.body.categoryName;

    const category = new Category ({
        categoriesName: categoryName
    });
    category.save()
    .then(result => {
        console.log(result);
        console.log('Created Categories');
        res.redirect('/add-category');
    }).catch(err => {
        console.log(err);
    });
}

