const path = require('path');

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');

const MONGODB_URI = 'mongodb+srv://joahan:3tG3RFzHFmyHJfAH@clustervricisely.wk85y.mongodb.net/seller?retryWrites=true&w=majority&ssl=true';

const app = express();
const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions', 
});
const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');  
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toString() + '-' + file.originalname);
    }
});

const fileFilter = (req, file, cb)=> { 
    if(file.mimetype === 'image/png' || file.mimetype === 'image/jpg' || file.mimetype === 'image/jpeg'){
        cb(null, true);
    }
    else {
        cb(null, false);
    }
}

app.set('view engine', 'ejs');
app.set('views', 'views')

const errorController = require('./controllers/error');
const User = require('./models/user');

const adminRoutes = require('./routes/admin');
const sellerRoutes = require('./routes/seller');
const authRoutes = require('./routes/auth');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(multer({storage: fileStorage, fileFilter: fileFilter }).single('images'));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/images',express.static(path.join(__dirname, 'images')));
app.use(
    session({
        secret: 'my secret', 
        resave: false, 
        saveUninitialized: false,
        store: store
    })
);
app.use(csrfProtection);
app.use(flash());

//Untuk set cek auth dan memberikan CSRF token
app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
})

app.use((req, res, next) => {
    // throw new Error('sync dummy');
    if(!req.session.user){
        return next();
    }
    User.findById(req.session.user._id)
    .then(user => {
        if(!user){
            return next();
        }
        req.user = user;
        next();
    }) 
    .catch(err => {
        next(new Error(err));
    });
})

app.use('/admin',adminRoutes);
app.use(sellerRoutes);
app.use(authRoutes);

app.get('/500', errorController.get500);
app.use(errorController.get404);

//middleware untuk menangani errors
app.use((error, req, res, next)=> {
    // res.redirect('/500');
    res.status(500).render('500', {
        pageTitle: 'Error', 
        path: '500',
        isAuthenticated: req.session.isLoggedIn
    });
})

mongoose
.connect(MONGODB_URI)
.then(result => {
    app.listen(4000);
})
.catch(err => {
    console.log(err);
});
