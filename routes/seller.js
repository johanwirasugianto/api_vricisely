const path = require('path');

const express = require('express');

const sellerController = require('../controllers/seller');
const isAuth = require('../middleware/is-auth');
const {body} = require('express-validator/check');
const uploadWakeh = require("../middleware/multer");

const router = express.Router();

router.get('/',isAuth, sellerController.getHome);

router.post('/addBrands',isAuth, sellerController.postAddBrand);

router.get('/store/:brandId',isAuth, sellerController.getStore);

router.get('/addProduct/:brandId',isAuth, sellerController.getAddProduct);

router.post('/addProduct',isAuth, sellerController.postaddProduct);

router.get('/brandProfile/:brandId',isAuth, sellerController.getBrandProfile);

router.post('/editBrand',isAuth, sellerController.postEditBrandProfile);

router.post('/deleteProduct',isAuth, sellerController.postDeleteProduct);

router.post('/brandBanner1', sellerController.postBrandBanner1);
router.post('/deleteBrandBanner1', sellerController.postDeleteBanner1);
router.post('/brandBanner2', sellerController.postBrandBanner2);
router.post('/deleteBrandBanner2', sellerController.postDeleteBanner2);
router.post('/brandBanner3', sellerController.postBrandBanner3);
router.post('/deleteBrandBanner3', sellerController.postDeleteBanner3);

router.get('/editProduct/:productId/:categoryId', sellerController.getEditProducts);
router.post('/editProduct', sellerController.postEditProduct);

router.post('/imagesProduct1', sellerController.postImagesProduct1);
router.post('/imagesProduct2', sellerController.postImagesProduct2);
router.post('/imagesProduct3', sellerController.postImagesProduct3);
router.post('/imagesProduct4', sellerController.postImagesProduct4);
router.post('/imagesProduct5', sellerController.postImagesProduct5);
router.post('/imagesProduct6', sellerController.postImagesProduct6);
router.post('/imagesProduct7', sellerController.postImagesProduct7);
router.post('/imagesProduct8', sellerController.postImagesProduct8);

router.post('/deleteImagesProduct1', sellerController.postDeleteImagesProduct1);
router.post('/deleteImagesProduct2', sellerController.postDeleteImagesProduct2);
router.post('/deleteImagesProduct3', sellerController.postDeleteImagesProduct3);
router.post('/deleteImagesProduct4', sellerController.postDeleteImagesProduct4);
router.post('/deleteImagesProduct5', sellerController.postDeleteImagesProduct5);
router.post('/deleteImagesProduct6', sellerController.postDeleteImagesProduct6);
router.post('/deleteImagesProduct7', sellerController.postDeleteImagesProduct7);
router.post('/deleteImagesProduct8', sellerController.postDeleteImagesProduct8);

//=======FIX=======\\

router.get('/products', sellerController.getProducts);

router.get('/products/:productId', sellerController.getProductId);

router.get('/add-product',isAuth , sellerController.getAddProducts);

// router.post(
//     '/add-products',
//     [
//         body('productName')
//             .isString()
//             .isLength({min:3})
//             .trim(),
//         // body('imageUrl').isURL(),
//         body('description')
//         .isLength({min:5, max:'400'})
//         .trim(),
//         body('color').trim(),
//     ],
//     isAuth, 
//     sellerController.postAddProduct);

// router.get('/edit-product/:productId',isAuth , sellerController.getEditProducts);

// router.post(
//     '/edit-product',
//     isAuth,
//     [
//         body('productName')
//             .isString()
//             .isLength({min:3})
//             .trim(),
//         // body('imageUrl').isURL(),
//         body('description')
//         .isLength({min:5, max:'400'})
//         .trim(),
//         body('color').trim(),
//         // body('sizeS').isNumeric(),
//         // body('sizeM').isNumeric(),
//         // body('sizeL').isNumeric(),
//         // body('sizeXL').isNumeric(),     
//         // body('tokopedia').isURL(),
//         // body('shopee').isURL(),
//         // body('bukalapak').isURL(),
//         // body('instagram').isURL(),
//     ], 
//     sellerController.postEditProduct);

// router.post('/delete-product',isAuth , sellerController.postDeleteProduct);

// // router.get('/coba', sellerController.getCoba);

router.get('/brand',isAuth , sellerController.getBrand);

// router.get('/add-brand' , sellerController.getAddBrand);

router.post('/add-brand' , sellerController.postAddBrand);

router.post('/delete-brand',isAuth, sellerController.postDeleteBrand);


//ADD Category
router.get('/add-Category',isAuth, sellerController.getAddCategory);
router.post('/add-Category',isAuth, sellerController.postAddCategory);

module.exports = router;
