const path = require('path');

const express = require('express');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product',isAuth , adminController.getAddProduct);

// /admin/product => GET
router.get('/products',isAuth , adminController.getProduct);

// /admin/add-product => POST
router.post('/add-product',isAuth , adminController.postAddProduct);


//Kembalikan data ke app.js
module.exports = router;

// exports.routes = router;
// exports.product = product;
