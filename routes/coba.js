const path = require('path');
const express = require('express');

const router = express.Router();

router.get('/coba', (req, res, next) => {
    res.render('coba', {
        pageTitle: 'Cuman Coba', 
        path: '/coba', 
    })
});

module.exports = router;